

CXX = gcc
CFLAGS = -g -Wall 
LD = gcc
LFLAGS = -lglfw3 -lstdc++ -lm -lGL -lX11 -lXi -lXxf86vm -lXinerama -lXcursor -lrt -lassimp -lpthread -ldl
EXE = engine

OBJS = build/opengl.o build/camera.o build/window.o build/glad.o

.PHONY: all clean

all: $(OBJS) $(EXE)

engine: $(OBJS)
	$(LD) $(OBJS) -o $(EXE) $(LFLAGS) 

build/opengl.o: src/opengl.cpp
	$(CXX) $(CFLAGS) -c src/opengl.cpp -o build/opengl.o

build/camera.o: src/graphics/camera.cpp src/graphics/camera.h
	$(CXX) $(CFLAGS) -c src/graphics/camera.cpp -o build/camera.o

build/window.o: src/graphics/window.cpp src/graphics/window.h
	$(CXX) $(CFLAGS) -c src/graphics/window.cpp -o build/window.o

build/glad.o: src/graphics/glad/glad.c src/graphics/glad/glad.h
	$(CXX) $(CFLAGS) -c src/graphics/glad/glad.cpp -o build/glad.o