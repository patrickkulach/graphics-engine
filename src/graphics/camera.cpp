#include <glm/vec3.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/glm.hpp>
#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include <iostream>
#include "camera.h"

#define PI (double) 3.1415926535897932384626433
// This file contains all camera mathematical functions
// This includes everything that controls the camera

glm::mat4 view = glm::mat4(1.0f);

extern int WIDTH; extern int HEIGHT;
float lastX = WIDTH / 2; float lastY = HEIGHT / 2; //Initialize the last x and y to half the size of the screen

namespace cam {
//Class variables
glm::vec3 cameraPosition(0.0f, 0.0f, -1.0f); //Initialize position to origin
glm::vec3 cameraDirection(0.0f, 0.0f, 1.0f); // Initialize direction to -z position


static double sensitivity = .01;
static double mouseSensitivity = .002;
static double speed = .04;
static double yaw = 0.0;
static double pitch = 0.0;

static bool mouseCaptured = true; //Determines if mouse is captured

//Takes the input and processes it to make a new camera position and direction
void updateCamera() { 
    //std::cout << "(" << yaw << ", " << pitch << ") (" << cameraPosition.x << ", " << cameraPosition.y << ", " << cameraPosition.z << ")" << std::endl;
    //Uncomment above to debug camera

    view = glm::mat4(1.0f);
    cameraDirection = glm::vec3(-cos(yaw) * cos(pitch), -sin(pitch), -sin(yaw) * cos(pitch)); //Camera dir is set, direction is pointing opposite of where camera points
    cameraDirection = glm::normalize(cameraDirection);

    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f); //Not real camera Up, this is only used for a cross product trick for the right vector
    glm::vec3 cameraRight = glm::normalize(glm::cross(cameraUp, cameraDirection));
    cameraUp = glm::normalize(glm::cross(cameraDirection, cameraRight)); //cameraUp is right cross up

    view = glm::lookAt(cameraPosition, cameraPosition + cameraDirection, cameraUp);

}


//These next two functions are used to return the view matrices needed for 3D viewing, with
//an input amp which will be half the distance between the left and right cameras
glm::mat4 right3DViewMat(float amp) { 
    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f); //Not real camera Up, this is only used for a cross product trick for the right vector
    glm::vec3 cameraRight = glm::normalize(glm::cross(cameraUp, cameraDirection));
    cameraUp = glm::normalize(glm::cross(cameraDirection, cameraRight)); //cameraUp is right cross up

    glm::vec3 cameraOffset = amp * glm::vec3(-sin(yaw), 0.0f, cos(yaw));
    return glm::lookAt(cameraPosition + cameraOffset, cameraPosition + cameraDirection, cameraUp);
}

glm::mat4 left3DViewMat(float amp) {
    glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f); //Not real camera Up, this is only used for a cross product trick for the right vector
    glm::vec3 cameraRight = glm::normalize(glm::cross(cameraUp, cameraDirection));
    cameraUp = glm::normalize(glm::cross(cameraDirection, cameraRight)); //cameraUp is right cross up

    glm::vec3 cameraOffset = amp * glm::vec3(-sin(yaw), 0.0f, cos(yaw));
    return glm::lookAt(cameraPosition - cameraOffset, cameraPosition + cameraDirection, cameraUp);
}

//Function to process input, not a callback
void processInput(GLFWwindow *win) {
    if (glfwGetKey(win, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(win, true);
    }

    //Pitch and yaw value updates
    if (glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS) {
        cam::yaw -= sensitivity;
    }
    if (glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS) {
        cam::yaw += sensitivity;
    }
    if (glfwGetKey(win, GLFW_KEY_DOWN) == GLFW_PRESS) {
        cam::pitch += sensitivity;
        if (cam::pitch >= PI / 2) cam::pitch = PI / 2;
    }
    if (glfwGetKey(win, GLFW_KEY_UP) == GLFW_PRESS) {
        cam::pitch -= sensitivity;
        if (cam::pitch <=  -PI / 2) cam::pitch = -PI / 2;
    }

    //Change direction
    //In our coordinite system, yaw = 0 means facing east

    if (glfwGetKey(win, GLFW_KEY_W) == GLFW_PRESS) {
        cam::cameraPosition -= glm::vec3(speed * cos(yaw), 0, speed * sin(yaw));
    }
    if (glfwGetKey(win, GLFW_KEY_S) == GLFW_PRESS) {
        cam::cameraPosition += glm::vec3(speed * cos(yaw), 0, speed * sin(yaw));
    }
    if (glfwGetKey(win, GLFW_KEY_A) == GLFW_PRESS) {
        cam::cameraPosition -= glm::vec3(speed * sin(yaw), 0, speed * -cos(yaw));
    }
    if (glfwGetKey(win, GLFW_KEY_D) == GLFW_PRESS) {
        cam::cameraPosition += glm::vec3(speed * sin(yaw), 0, speed * -cos(yaw));
    }
    if (glfwGetKey(win, GLFW_KEY_SPACE) == GLFW_PRESS) {
        cam::cameraPosition += glm::vec3(0, speed, 0);
    }
    if (glfwGetKey(win, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        cam::cameraPosition += glm::vec3(0, -speed, 0);
    }
    if (glfwGetKey(win, GLFW_KEY_F1) == GLFW_PRESS) {
        if (mouseCaptured) 
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL); //Reenable cursor for activities
        else 
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_DISABLED); //Reenable cursor for activities
        mouseCaptured = !mouseCaptured;
    }

}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    float xOffset = xpos - lastX;
    float yOffset = ypos - lastY; 
    lastX = xpos;
    lastY = ypos;
    
    yaw += xOffset * mouseSensitivity;
    pitch += yOffset * mouseSensitivity;
    if (cam::pitch >= PI / 2) cam::pitch = PI / 2; //Place limit on pitch values
    if (cam::pitch <=  -PI / 2) cam::pitch = -PI / 2;
        
    long winl = (long) window; //This line just prevents the warning "win is not used" I don't know if I can safely get rid of win though
    winl += 1;
    
}

} //End of namespace