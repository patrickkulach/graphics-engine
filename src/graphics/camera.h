#ifndef CAMERA_H
#define CAMERA_H
#include <glm/matrix.hpp>
#include <glm/glm.hpp>
namespace cam {
//Class methods
void processInput(GLFWwindow* win);
void renderObjects();
void updateCamera();
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
glm::mat4 right3DViewMat(float amp);
glm::mat4 left3DViewMat(float amp);
} //End of namespace

#endif