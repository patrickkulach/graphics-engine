// This file will be used to create a mesh that represents 3D modelling
// It will have a struct that contains all the mesh data, and the normals, along
// with its own drawing function. It will also contain the functions necessary to create these
// meshes and normals.

#include <vector>
#include "../graphics/shader.h"

class Terrain {

    public:
        //Terrain();
        void draw(Shader s);
        void generateMesh(float values[]);
        std::vector<float> generateValues(int n, float x1, float z1, float x2, float z2);
    private:
        float f(float x, float z);
        float* vertexData;
        int vertexDataLength;
        float* normalData;
        int normalDataLength;
        int xnum, znum; //Number of vertices in the z direction and in the x direction
        unsigned int VAO, VBO, EBO; 
    
    public:
        Terrain() {
            xnum = 0;
            znum = 0;
        }
        // This class takes in 6 parameters and returns an array of values that represent the y values for the (x,z) coordinate in that position
        // It will also set xnum and znum parameters, which will be useful once the mesh is generated. How the return float[] works is that it 
        // is a matrix placed into an array. After xnum spaces, the next xcoordinate begins. This goes until the final value of x (x2), and the last 
        // index in the array will represent f(x2,z2)
        std::vector<float> generateValues(int xn, int zn, float x1, float z1, float x2, float z2) {
            float xDist = (x2 - x1) / (float) (xn - 1); // The -1 is because of a common problem called the fence post error
            float zDist = (z2 - z1) / (float) (zn - 1); // Anyway, what these two values represent is the distance between each z Coordinate or x Coordinate
            std::vector<float> values(xn * zn); // We need xn * zn values for the entire array

            // We now calculate the value and load the array
            // i will be the x direction, j will be the z direction
            for (int i = 0; i < xn; i++) {
                for (int j = 0; j < zn; j++) {
                    float xval = i * xDist + x1;
                    float zval = j * zDist + z1;
                    int index = i * xn + j;

                    values[index] = f(xval, zval);

                }
            }
            return values;
        }

};

int testingmain() {
    Terrain testTerrain();
    //std::vector<float> testvec = testTerrain->generateValues(2, 2, 0.0f, 0.0f, 1.0f, 1.0f);
    std::cout << "Hello there" << std::endl;


}