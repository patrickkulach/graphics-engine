//All this code was copied from the website

#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec2 TexCoords;
out vec3 normal;
out float zPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 perspective;

void main()
{
    TexCoords = aTexCoords;    
    //gl_Position = projection * view * model * vec4(aPos, 1.0);
    gl_Position = perspective * view * model * vec4(aPos,1.0);
    zPos = gl_Position.z;
    normal = aNormal;
}

