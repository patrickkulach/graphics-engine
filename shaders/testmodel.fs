//Also copied

#version 330 core
out vec4 FragColor;

in vec2 TexCoords;
in vec3 normal; //The normal vector for the fragment
in float zPos;

uniform sampler2D texture_diffuse1;

vec3 objectColor = vec3(0.5f, 0.8f, 1.0f); 

vec3 sunPos = normalize(vec3(1.0f, 1.0f, 1.0f)); //The position of the direction of the sunlight... what else
vec3 sunColor = vec3(1.0f, 1.0f, 1.0f); //The color of the sunlight, white as default

vec3 ambient = vec3(.2f, .2f, .2f); //The absolute minimum the light can be
vec3 diffuse = vec3(.6f, .6f, .6f); //The effectiveness of diffuse lighting

void main()
{    
    vec3 diffuseLight = max(0.0f, dot(sunPos, normal)) * diffuse; 

    FragColor = vec4((diffuseLight + ambient) * objectColor, 1.0f);
    //FragColor = texture(texture_diffuse1, TexCoords);
    //FragColor = vec4(zPos * .75f, zPos * .5f, zPos * .25f, 1.0f);
}

